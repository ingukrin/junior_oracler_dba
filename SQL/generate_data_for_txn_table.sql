SET SERVEROUTPUT ON;

DECLARE
    v_counter          NUMBER := 0;
    v_commit_counter   NUMBER := 0;
BEGIN
    dbms_output.put_line('Start data insert--> '||to_char(sysdate,'YYYY.MM.DD hh24:mi:ss'));        
    LOOP
        v_counter := v_counter + 1;
        v_commit_counter := v_commit_counter + 1;
        -- Insert hr.transactions table 1-st row sequence, time(modified), and generated text by uuid
        INSERT INTO hr.transactions (
            txn_id,
            txn_date,
            data
        ) VALUES (
            --v_commit_counter,
            hr.txn_seq.nextval,
            sysdate - 500 + 0.1 * v_commit_counter,
            sys_guid()
        );

--- commit every 10000 record
        IF v_commit_counter = 10000 THEN
            v_commit_counter := 0;
            COMMIT;
        END IF;
        
--- if there was inserted 1000 000 records, then  exit;        
        IF v_counter = 1000000 THEN
            EXIT;
        END IF;
    END LOOP;

    COMMIT;
 dbms_output.put_line('All data was inserted at--> '||to_char(sysdate,'YYYY.MM.DD hh24:mi:ss'));        
END;
/

exit;