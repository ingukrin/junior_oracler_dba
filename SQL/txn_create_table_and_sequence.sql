DROP TABLE hr.transactions;
DROP SEQUENCE hr.txn_seq;


CREATE TABLE hr.transactions (
    txn_id     NUMBER
        NOT NULL ENABLE,
    txn_date   DATE
        NOT NULL ENABLE,
    data       VARCHAR2(100 BYTE)
        NOT NULL ENABLE
);

COMMENT ON COLUMN hr.transactions.txn_id IS
    'Transaction id';

COMMENT ON COLUMN hr.transactions.txn_date IS
    'Transaction date';

COMMENT ON COLUMN hr.transactions.data IS
    'Infomation about transactions';

CREATE SEQUENCE hr.txn_seq MINVALUE 100000 MAXVALUE 5000000 INCREMENT BY 1 START WITH 100000 CACHE 20 ORDER NOCYCLE NOKEEP NOSCALE
GLOBAL;
commit;
/


exit;