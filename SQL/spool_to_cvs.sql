SET COLSEP ","
SET TERM OFF
SET FEED OFF
SET HEADSEP OFF
SET PAGESIZE 0
SET TRIMSPOOL ON
SET LINESIZE 500
SET NUMWIDTH 10


SPOOL "/tmp/gathered_data.csv"

SELECT
    txn_id,
    to_char(txn_date,'YYYY-MM-DD hh24:mi:ss') txn_date,	    
    data
FROM
    hr.transactions
WHERE     
    ROWNUM <= 100;

SPOOL OFF;

EXIT
/
