LOG_DIR="$HOME/MAKETASK/log"
LOG_FILE=`basename "$0"`

LOG="$LOG_DIR/$LOG_FILE.log"

make_log(){
echo `date "+%Y-%m-%d %I:%M:%S"` $1 >> $LOG
}


make_log "Export ORACLE libraries"
export PATH="$PATH:/opt/oracle/instantclient_19_8"
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/opt/oracle/instantclient_19_8"

make_log "Spool data to cvs"
sqlplus hr/hr@HRDB @SQL/spool_to_cvs.sql >> $LOG




