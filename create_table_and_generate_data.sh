
LOG_DIR="$HOME/MAKETASK/log"
LOG_FILE=`basename "$0"`

LOG="$LOG_DIR/$LOG_FILE.log"

make_log(){
echo `date "+%Y-%m-%d %I:%M:%S"` $1 >> $LOG
}


make_log "Export ORACLE libraries"
export PATH="$PATH:/opt/oracle/instantclient_19_8"
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/opt/oracle/instantclient_19_8"

make_log "Create database and tables"
sqlplus hr/hr@HRDB @SQL/txn_create_table_and_sequence.sql >> $LOG

make_log "Generate data in database"
sqlplus hr/hr@HRDB @SQL/generate_data_for_txn_table.sql >> $LOG 



