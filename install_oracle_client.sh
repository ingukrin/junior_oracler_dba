#!/bin/bash

LOG_DIR="$HOME/MAKETASK/log"
LOG_FILE=`basename "$0"`
LOG="$LOG_DIR/$LOG_FILE.log"

make_log(){
echo `date "+%Y-%m-%d %I:%M:%S"` $1 >> $LOG
}

if [ ! -d "/opt/oracle" ]; then
        make_log "Create directory for orecle clien"
        sudo mkdir /opt/oracle
else
        make_log "/opt/oracle already exists"
fi
if [ ! -d /opt/oracle/instantclient_19_8 ]; then

        make_log "Copy oracle intalation to destination folder"
        sudo cp /home/ingus/DOWNLOAD/*.zip /opt/oracle
        cd /opt/oracle

        make_log "unzip intalation files"
        sudo unzip instantclient-basic-linux.x64-19.8.0.0.0dbru.zip
        sudo unzip instantclient-sqlplus-linux.x64-19.8.0.0.0dbru.zip
        sudo rm *.zip
else
        make_log "ORA_CLIENT direcotry already exists. Skip ORA client instalation"
fi
if [ ! -f "/opt/oracle/instantclient_19_8/network/admin/tnsnames.ora" ];then
        make_log "Copy tnsnames.ora file"
        sudo cp /home/ingus/DOWNLOAD/*.ora /opt/oracle/instantclient_19_8/network/admin
else
        make_log "tnsnames.ora already exists"
fi
